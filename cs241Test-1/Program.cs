﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs241Test_1
{

    public interface IBookend
    {
        bool IsStartPoint(out char bookend);

        char TheChar { get; set; }

    }

    public class BookendChar : IBookend
    {
        public BookendChar(char theChar)
        {
            TheChar = theChar;
            hasBookend = false;
        }
        public BookendChar(char theChar, char theBookend)
        {
            TheChar = theChar;
            TheBookend = theBookend;
            hasBookend = true;
        }

        bool IBookend.IsStartPoint(out char bookend)
        {
            bool returnValue = false;
            bookend = ' ';

            if (hasBookend)
            {
                returnValue = true;
                bookend = TheBookend;
            }
            return returnValue;
        }

        public bool hasBookend { get; private set; }
        public char TheChar { get; set; }
        public char TheBookend { get; set; }
    }
    public class Program
    {
        public static string reverse(string thingToReverse)
        {
            string newString ="";

            // If the first char of the string is an opening or closing parenthesis, remove and reverse it
            if(thingToReverse.First() == '(')
            {
                thingToReverse = thingToReverse.Remove(0,1);
                thingToReverse = ')' + thingToReverse;
            }
            else if(thingToReverse.First() == ')')
            {
                thingToReverse = thingToReverse.Remove(0,1);
                thingToReverse = '(' + thingToReverse;
            }

            if(thingToReverse.Count() != 1)
            {
                newString = reverse(thingToReverse.Substring(1));
            }
            newString = newString + string.Concat(thingToReverse.First());

            return newString;
        }

        public static string parenthesisReverse(string thingToReverse)
        {
            string newString = "";
            
            // If we encounter a parenthesis, reverse the string in the parenthesis first
            if(thingToReverse.First() == '(')
            {
                newString = reverse(thingToReverse.Substring(1, thingToReverse.IndexOf(')') - 1));
            }
            newString = '(' + newString + ')';

            return newString;
        }



        static void Main(string[] args)
        {
        }
    }
}
